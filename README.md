# OSU Clone WIP 

A rhythm game like OSU! with only circles, made in java
Gameplay: move mouse over the circle and hit X or Z based on the rhythm of the music selected

Some monitors or HMDI cables may cause latency issues. Trun on the Video Mode option in the settings to compensate (user defined offset is not yet implemented)

The game is compatible with most OSU! standard maps. To create a new map, just copy everything from the HitObjects field of the original .osu file and the song as "objects.txt" and "audio.wav" respectively into a new folder, then move this folder into the maps directory
