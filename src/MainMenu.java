import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.stream.Stream;

public class MainMenu extends JFrame {

    private int approachRate;
    private int circleSize;
    private boolean videoMode;
    private final MainMenu mm;
    private JFileChooser chooser;
    private String mapPath;

    public MainMenu() {
        mm = this;
        approachRate = 3;
        circleSize = 150;


        JPanel panel = new JPanel();
        panel.setBackground(Color.BLACK);
        panel.setLayout(null);
        panel.repaint();
        setLayout(null);
        add(panel);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        panel.setBounds(0, 0, (int) screenSize.getWidth(), (int) screenSize.getHeight());
        JButton start = new JButton("Start");
        JButton settings = new JButton("Settings");
        JButton quit = new JButton("Quit");
        JButton select = new JButton("Select Map");
        JButton scores = new JButton("Saved Scores");
        panel.add(start);
        panel.add(settings);
        panel.add(quit);
        panel.add(select);
        panel.add(scores);
        start.setBounds(890, 300, 200, 100);
        select.setBounds(890, 500, 200, 100);
        scores.setBounds(890, 700, 200, 100);
        settings.setBounds(500, 900, 200, 100);
        quit.setBounds(1280, 900, 200, 100);
        Stream.of(start, settings, quit, select, scores).forEach(x -> {
            x.setBackground(Color.WHITE);
            x.setForeground(Color.RED);
        });

        start.addActionListener(e -> {
            mm.dispose();
            Circles circles = new Circles();
            BufferedReader br;
            try {
                br = new BufferedReader(new FileReader(mapPath + "\\objects.txt"));
                String line = null;
                try {
                    line = br.readLine();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                while (line != null) {
                    String[] dat = line.split(",");
                    int x = Integer.parseInt(dat[0]);
                    int y = Integer.parseInt(dat[1]);
                    int time = Integer.parseInt(dat[2]);
                    try {
                        circles.add(new HitCircle(time, x * 2 + 150, y * 2 + 70, circleSize));
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                    try {
                        line = br.readLine();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
                try {
                    new GamePlay(circles, approachRate, mm);
                } catch (IOException | UnsupportedAudioFileException | LineUnavailableException ioException) {
                    ioException.printStackTrace();
                }
            } catch (FileNotFoundException fileNotFoundException) {
                mm.setVisible(true);
                JOptionPane.showMessageDialog(mm, "Select a map first!");
            }
        });

        quit.addActionListener(e -> {
            mm.dispose();
            System.exit(0);
        });

        settings.addActionListener(e -> new Settings(mm));

        select.addActionListener(e -> {
            chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File(".\\maps"));
            chooser.setDialogTitle("Choose map directory");
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            chooser.setAcceptAllFileFilterUsed(false);
            if (chooser.showOpenDialog(mm) == JFileChooser.APPROVE_OPTION) {
                mapPath = chooser.getSelectedFile().toString();
            } else {
                System.out.println("No Selection ");
            }
        });

        scores.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFrame frame = new JFrame();
                try {
                    Scores scores1 = new Scores(mm);
                    frame.add(scores1);
                    frame.setBounds(50, 50, 1400, 1000);
                    frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                    frame.setVisible(true);
                    frame.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyTyped(KeyEvent e) {
                            super.keyTyped(e);
                            if (e.getKeyChar() == 'c') {
                                FileWriter fw;
                                try {
                                    fw = new FileWriter(mapPath + "\\scores.txt");
                                    fw.write(mapPath.split("maps")[1]);
                                    fw.close();
                                } catch (IOException ioException) {
                                    ioException.printStackTrace();
                                }
                                scores1.repaint();
                            }

                        }
                    });
                } catch (FileNotFoundException fileNotFoundException) {
                    JOptionPane.showMessageDialog(mm, "Select a map first!");
                }
            }
        });

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    public int getApproachRate() {
        return approachRate;
    }

    public void setApproachRate(int approachRate) {
        this.approachRate = approachRate;
    }

    public int getCircleSize() {
        return circleSize;
    }

    public void setCircleSize(int circleSize) {
        this.circleSize = circleSize;
    }

    public boolean isVideoMode() {
        return videoMode;
    }

    public void setVideoMode(boolean videoMode) {
        this.videoMode = videoMode;
    }

    public String getMapPath() {
        return mapPath;
    }
}
