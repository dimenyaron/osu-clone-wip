import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class GamePlay extends JFrame {

    public GamePlay(Circles c, int approachRate, MainMenu mm) throws IOException, UnsupportedAudioFileException, LineUnavailableException {

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        toolkit.getBestCursorSize(30, 30);
        Image image = toolkit.getImage("assets\\tp.png");
        Cursor cursor = toolkit.createCustomCursor(image, new Point(15, 15), "custom");
        setCursor(cursor);

        AudioInputStream audioInputStream2 = AudioSystem.getAudioInputStream(
                new File(mm.getMapPath() + "\\audio.wav"));
        Clip music = AudioSystem.getClip();
        music.open(audioInputStream2);

        HitSound[] hitSounds = new HitSound[c.size()];
        ViewHitCircle vhc = new ViewHitCircle(c);
        ControllerHitCircle[] controllerHitCircles = new ControllerHitCircle[c.size()];
        Thread[] threads = new Thread[c.size()];
        for (int i = 0; i < c.size(); i++) {
            hitSounds[i] = new HitSound();
            controllerHitCircles[i] = new ControllerHitCircle(c.get(i), vhc, approachRate, i);
            threads[i] = new Thread(controllerHitCircles[i]);
        }
        KeyPress keyPress = new KeyPress(c, 0, 0, hitSounds);
        addKeyListener(keyPress);


        c.get(0).setActive(true);
        int size = (int) c.get(0).getSize();

        addMouseMotionListener(new MouseMotionListener() {
            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {
                keyPress.setX(e.getX());
                keyPress.setY(e.getY());
                vhc.MousePointer(e.getX(), e.getY());
            }
        });

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == 27) {
                    music.stop();
                    dispose();
                    mm.setVisible(true);
                    JFrame frame = new JFrame();
                    ScoreScreen scoreScreen = new ScoreScreen(vhc);
                    frame.add(scoreScreen);
                    frame.setBounds(50, 50, 1000, 1000);
                    frame.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                    frame.setVisible(true);
                    frame.addKeyListener(new KeyAdapter() {
                        @Override
                        public void keyTyped(KeyEvent e) {
                            super.keyTyped(e);
                            if (e.getKeyChar() == 's') {
                                File file = new File(mm.getMapPath() + "\\scores.txt");
                                try {
                                    FileWriter fw = new FileWriter(file, true);
                                    fw.write("\nRank: " + scoreScreen.getRank() + " ,score: " + scoreScreen.getScore() + " ,percentage: " + scoreScreen.getPercentage() + "% , Approach Rate: " + approachRate + " , Circle Size: " + c.get(0).getSize());
                                    fw.close();
                                    frame.dispose();
                                } catch (IOException ioException) {
                                    ioException.printStackTrace();
                                }
                            }
                        }
                    });
                }
            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        add(vhc);

        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setUndecorated(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setVisible(true);

        new Thread(() -> {
            try {
                Thread.sleep(5000);
                Arrays.stream(threads).forEach(Thread::start);
                new Thread(() -> {
                    if (mm.isVideoMode()) {
                        try {
                            Thread.sleep(size / 2 * approachRate + 200);
                            music.start();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    else {
                        try {
                            Thread.sleep(size / 2 * approachRate);
                            music.start();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();
    }

}
