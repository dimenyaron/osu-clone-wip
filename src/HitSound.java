import javax.sound.sampled.*;
import java.io.File;
import java.io.IOException;

public class HitSound {
    private final Clip clip;

    public HitSound() throws IOException, UnsupportedAudioFileException, LineUnavailableException {
        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(
                new File("assets\\hitsound.wav"));
        clip = AudioSystem.getClip();
        clip.open(audioInputStream);
        FloatControl gainControl =
                (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
        gainControl.setValue(-5.0f);
    }

    public Clip getClip() {
        return clip;
    }
}
