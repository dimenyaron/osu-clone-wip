import javax.swing.*;
import java.awt.*;
import java.io.*;

public class Scores extends JPanel {
    private final BufferedReader br;

    public Scores(MainMenu mm) throws FileNotFoundException {
        File file = new File(mm.getMapPath() + "\\scores.txt");
        br = new BufferedReader(new FileReader(file));
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("TimesRoman", Font.PLAIN, 25));
        int height = 50;
        try {
            String line = br.readLine();
            while (line != null) {
                g2d.drawString(line, 100, height);
                height += 50;
                line = br.readLine();
            }
            g2d.drawString("Press \"c\" to clear scores", 100, height);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
