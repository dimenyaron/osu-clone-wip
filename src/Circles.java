import java.util.ArrayList;

public class Circles extends ArrayList<HitCircle> {
    public ArrayList<Integer> active() {
        ArrayList<Integer> act = new ArrayList<>();
        for (int i = 0; i < this.size(); i++) {
            if (this.get(i).isActive()) {
                act.add(i);
            }
        }
        return act;
    }
}
