import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class ViewHitCircle extends JPanel {
    private final Circles circles;
    private int[] blocked;
    private int[] pointSet;
    private final Image _0;
    private final Image _50;
    private final Image _100;
    private int x;
    private int y;
    private final Image cursor;
    private int point;
    private int combo;
    private int maxCombo;
    private final int circleNumber;
    private int miss;
    private int fifty;
    private int hundred;
    private int threeHundred;

    public ViewHitCircle(Circles circles) throws IOException {
        this.circles = circles;
        this.blocked = new int[circles.size()];
        this.pointSet = new int[circles.size()];
        circleNumber = circles.size();
        blocked = Arrays.stream(blocked).map(x -> x = 1).toArray();
        pointSet = Arrays.stream(pointSet).map(x -> x = 0).toArray();
        _0 = ImageIO.read(new File("assets\\hit0.png"));
        _50 = ImageIO.read(new File("assets\\hit50.png"));
        _100 = ImageIO.read(new File("assets\\hit100.png"));
        cursor = ImageIO.read(new File("assets\\cursor.png"));
        combo = 0;
        maxCombo = 0;
        point = 0;
        miss = circleNumber;
        fifty = 0;
        hundred = 0;
        threeHundred = 0;
    }

    public void setVisible(int i) {
        blocked[i] = 0;
    }

    public void setFade(int i) {
        blocked[i] = 1;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
        for (int i = 0; i < circles.size(); i++) {
            if (blocked[i] != 1) {
                HitCircle circle = circles.get(i);
                if (circle.getPoint() == -1) {
                    if(i>0) {
                        if(Point.distance(circle.getX()+  circle.getSize() / 2 - 10,circle.getY()+  circle.getSize() / 2 + 10,circles.get(i-1).getX()+  circle.getSize() / 2 - 10,circles.get(i-1).getY()+  circle.getSize() / 2 + 10) > 400) {
                            g2d.setColor(Color.WHITE);
                            g2d.drawLine(circle.getX()+ (int) circle.getSize() / 2 - 10,circle.getY()+ (int) circle.getSize() / 2 + 10,circles.get(i-1).getX()+ (int) circle.getSize() / 2 - 10,circles.get(i-1).getY()+ (int) circle.getSize() / 2 + 10);
                        }
                    }
                    g2d.draw(circle.getCircle());
                    g2d.drawImage(circle.getApproach(), (int) (circle.getX() - circle.getApproachSize() / 2 + circle.getSize() / 2), (int) (circle.getY() - circle.getApproachSize() / 2 + circle.getSize() / 2), (int) (circle.getApproachSize()), (int) circle.getApproachSize(), null);
                    if (i % 8 < 4) {
                        g2d.drawImage(circle.getHit(), (int) (circle.getX() - circle.getSize() / 2), (int) (circle.getY() - circle.getSize() / 2), (int) circle.getSize() * 2, (int) circle.getSize() * 2, null);
                    } else {
                        g2d.drawImage(circle.getHit2(), (int) (circle.getX() - circle.getSize() / 2), (int) (circle.getY() - circle.getSize() / 2), (int) circle.getSize() * 2, (int) circle.getSize() * 2, null);
                    }
                    g2d.setColor(Color.GREEN);
                    g2d.setFont(new Font("TimesRoman", Font.PLAIN, 40));
                    g2d.drawString(String.valueOf((i % 4) + 1), circle.getX() + (int) circle.getSize() / 2 - 10, circle.getY() + (int) circle.getSize() / 2 + 10);

                } else {
                    switch (circle.getPoint()) {
                        case 0:
                            g2d.drawImage(_0, circle.getX(), circle.getY(), null);
                            if (pointSet[i] == 0) {
                                combo = 0;
                                pointSet[i] = 1;
                            }
                            break;
                        case 50:
                            g2d.drawImage(_50, circle.getX(), circle.getY(), null);
                            if (pointSet[i] == 0) {
                                combo += 1;
                                if (combo > maxCombo) {
                                    maxCombo = combo;
                                }
                                point += combo * 50;
                                pointSet[i] = 1;
                                fifty += 1;
                                miss -= 1;
                            }
                            break;
                        case 100:
                            g2d.drawImage(_100, circle.getX(), circle.getY(), null);
                            if (pointSet[i] == 0) {
                                combo += 1;
                                if (combo > maxCombo) {
                                    maxCombo = combo;
                                }
                                point += combo * 100;
                                pointSet[i] = 1;
                                hundred += 1;
                                miss -= 1;
                            }
                            break;
                        case 300:
                            if (pointSet[i] == 0) {
                                combo += 1;
                                if (combo > maxCombo) {
                                    maxCombo = combo;
                                }
                                point += combo * 300;
                                pointSet[i] = 1;
                                threeHundred += 1;
                                miss -= 1;
                            }
                    }
                }
            }
        }
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("TimesRoman", Font.PLAIN, 40));
        g2d.drawString(String.valueOf(point), 1680, 100);
        g2d.drawString(combo + "x", 1680, 150);
        if(pointSet[circleNumber-1] == 1){
            g2d.drawString("Press Escape to see scores", 100, 100);
        }
        g2d.drawImage(cursor, x - 50, y - 50, 100, 100, null);
    }

    public void MousePointer(int x, int y) {
        this.x = x;
        this.y = y;
        repaint();
    }

    public int getPoint() {
        return point;
    }

    public int getMaxCombo() {
        return maxCombo;
    }

    public int getCircleNumber() {
        return circleNumber;
    }

    public int getMiss() {
        return miss;
    }

    public int getFifty() {
        return fifty;
    }

    public int getHundred() {
        return hundred;
    }

    public int getThreeHundred() {
        return threeHundred;
    }
}
