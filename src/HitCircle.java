import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.io.File;
import java.io.IOException;

public class HitCircle {
    private final Shape circle;
    private final double size;
    private final long time;
    private final int x;
    private final int y;
    private double approachSize;
    private double offset;
    private int point;
    private final Image hit;
    private final Image hit2;
    private final Image approach;
    private boolean active;

    public HitCircle(long time, int x, int y, int size) throws IOException {
        this.time = time;
        this.x = x;
        this.y = y;
        this.size = size;
        this.approachSize = size * 3;
        this.circle = new Ellipse2D.Double(x, y, size, size);
        hit = ImageIO.read(new File("assets\\hitcircle.png"));
        hit2 = ImageIO.read(new File("assets\\hitcircle2.png"));
        approach = ImageIO.read(new File("assets\\approachcircle.png"));
        offset = 0;
        point = -1;
        active = false;
    }

    public Shape getCircle() {
        return circle;
    }

    public double getSize() {
        return size;
    }

    public long getTime() {
        return time;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getX() {
        return x;
    }

    public Image getHit() {
        return hit;
    }

    public Image getApproach() {
        return approach;
    }

    public int getY() {
        return y;
    }

    public double getApproachSize() {
        return approachSize;
    }

    public void setApproachSize(double approachSize) {
        this.approachSize = approachSize;
        offset = (size * 3) - approachSize;
    }

    public double getOffset() {
        return offset;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Image getHit2() {
        return hit2;
    }
}
