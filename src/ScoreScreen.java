import javax.swing.*;
import java.awt.*;

public class ScoreScreen extends JPanel {
    private final int maxCombo;
    private final int score;
    private final int miss;
    private final int fifty;
    private final int hundred;
    private final int threeHundred;
    private final double percentage;
    private String rank;


    public ScoreScreen(ViewHitCircle vhc) throws HeadlessException {
        this.maxCombo = vhc.getMaxCombo();
        this.score = vhc.getPoint();
        this.miss = vhc.getMiss();
        this.fifty = vhc.getFifty();
        this.hundred = vhc.getHundred();
        this.threeHundred = vhc.getThreeHundred();
        double circleNum = vhc.getCircleNumber();
        this.percentage = (int) (((fifty + hundred * 2 + threeHundred * 3) / (circleNum * 3)) * 100);


    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
        g2d.setColor(Color.WHITE);
        g2d.setFont(new Font("TimesRoman", Font.PLAIN, 50));
        if (percentage == 100) {
            g2d.drawString("Rank: SS", 400, 100);
            rank = "SS";
        } else {
            if (miss == 0) {
                g2d.drawString("Rank: S", 400, 100);
                rank = "S";
            } else {
                if (percentage > 90) {
                    g2d.drawString("Rank: A", 400, 100);
                    rank = "A";
                } else {
                    if (percentage > 70) {
                        g2d.drawString("Rank: B", 400, 100);
                        rank = "B";
                    } else {
                        if (percentage > 50) {
                            g2d.drawString("Rank: C", 400, 100);
                            rank = "C";
                        } else {
                            g2d.drawString("Rank: D", 400, 100);
                            rank = "B";
                        }
                    }
                }

            }
        }
        g2d.drawString("Score: " + score, 400, 200);
        g2d.drawString("Max combo: " + maxCombo + "x", 400, 300);
        g2d.drawString("Miss: " + miss + "x", 400, 400);
        g2d.drawString("50: " + fifty + "x", 400, 500);
        g2d.drawString("100: " + hundred + "x", 400, 600);
        g2d.drawString("300: " + threeHundred + "x", 400, 700);
        g2d.drawString(percentage + "%", 450, 800);
        g2d.drawString("Press \"s\" to save score", 450, 900);

    }

    public String getRank() {
        return rank;
    }

    public int getScore() {
        return score;
    }

    public double getPercentage() {
        return percentage;
    }
}
