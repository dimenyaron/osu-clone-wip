public class ControllerHitCircle implements Runnable {
    private final HitCircle hitCircle;
    private final ViewHitCircle viewHitCircle;
    private final int approachRate;
    private final int index;

    public ControllerHitCircle(HitCircle hitCircle, ViewHitCircle viewHitCircle, int approachRate, int index) {
        this.hitCircle = hitCircle;
        this.viewHitCircle = viewHitCircle;
        this.approachRate = approachRate;
        this.index = index;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(hitCircle.getTime());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        viewHitCircle.setVisible(index);
        hitCircle.setActive(true);
        while (hitCircle.getApproachSize() > 0) {
            hitCircle.setApproachSize(hitCircle.getApproachSize() - 2);
            viewHitCircle.repaint();
            try {
                Thread.sleep(approachRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (hitCircle.getPoint() != -1) {
                break;
            }
        }
        if (hitCircle.getPoint() == -1) {
            hitCircle.setPoint(0);
            hitCircle.setActive(false);
        }
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        viewHitCircle.setFade(index);
        viewHitCircle.repaint();
    }
}
