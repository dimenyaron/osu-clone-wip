import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

public class KeyPress extends KeyAdapter {

    private final Circles circles;
    private int x;
    private int y;
    private final HitSound[] sound;


    public KeyPress(Circles circles, int x, int y, HitSound[] sound) {
        this.circles = circles;
        this.x = x;
        this.y = y;
        this.sound = sound;
    }


    @Override
    public void keyTyped(KeyEvent keyEvent) {
        char c = keyEvent.getKeyChar();
        Point mouseLoc = new Point(x, y);
        ArrayList<Integer> indexes = circles.active();
        if (indexes.isEmpty()) {
            return;
        }
        for (Integer index : indexes) {
            HitCircle current = circles.get(index);
            if ((c == 'z' || c == 'x') && (current.getCircle().contains(mouseLoc)) && current.isActive()) {
                sound[index].getClip().start();
                if (current.getOffset() < current.getSize() * 2 / 3 || current.getOffset() >= current.getSize() * 5 / 2) {
                    current.setPoint(50);
                } else {
                    if (current.getOffset() < current.getSize() * 4 / 3 || current.getOffset() >= current.getSize() * 2 + current.getSize() / 3) {
                        current.setPoint(100);
                    } else {
                        current.setPoint(300);
                    }
                }
                current.setActive(false);
                break;
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {

    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

}
