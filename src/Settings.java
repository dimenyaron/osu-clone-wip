import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Settings extends JFrame {

    private int approachRate;
    private int circleSize;
    private boolean videoM;


    public Settings(MainMenu mm) throws HeadlessException {
        approachRate = mm.getApproachRate();
        circleSize = mm.getCircleSize();
        videoM=mm.isVideoMode();
        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 700, 700);
        panel.setBackground(Color.WHITE);

        panel.setLayout(new GridLayout(4, 2));


        JLabel approachRateLabel = new JLabel("Approach Rate", JLabel.CENTER);
        approachRateLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JSlider slider = new JSlider(SwingConstants.HORIZONTAL, 1, 10, approachRate);
        slider.setAlignmentX(Component.CENTER_ALIGNMENT);
        slider.setMajorTickSpacing(1);
        slider.setPaintTicks(true);
        slider.setPaintLabels(true);

        JLabel circleSizeLabel = new JLabel("Circle Size", JLabel.CENTER);
        approachRateLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JSlider slider2 = new JSlider(SwingConstants.HORIZONTAL, 120, 250, circleSize);
        slider2.setAlignmentX(Component.CENTER_ALIGNMENT);
        slider2.setMajorTickSpacing(10);
        slider2.setMinorTickSpacing(5);
        slider2.setPaintTicks(true);
        slider2.setPaintLabels(true);
        String mapName;

        if (mm.getMapPath() != null) {
            mapName = mm.getMapPath().split("maps")[1];
        }
        else{
            mapName ="NO MAP SELECTED!!! May produce errors";
        }


        JLabel mapLabel = new JLabel("Selected Map:", JLabel.CENTER);
        approachRateLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel mapPath = new JLabel(mapName, JLabel.CENTER);
        approachRateLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        JLabel videoMode = new JLabel("Video mode:",JLabel.CENTER);
        videoMode.setAlignmentX(Component.CENTER_ALIGNMENT);

        JCheckBox video = new JCheckBox("",videoM);

        slider.addChangeListener(e -> {
            JSlider source = (JSlider) e.getSource();
            approachRate = source.getValue();
            mm.setApproachRate(approachRate);
        });

        slider2.addChangeListener(e -> {
            JSlider source = (JSlider) e.getSource();
            circleSize = source.getValue();
            mm.setCircleSize(circleSize);
        });

        video.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                  if(video.isSelected()) {
                      mm.setVideoMode(true);
                      videoM=true;
                  }
                  else{
                      mm.setVideoMode(false);
                      videoM=false;
                  }
            }
        });


        panel.add(approachRateLabel);
        panel.add(slider);
        panel.add(circleSizeLabel);
        panel.add(slider2);
        panel.add(mapLabel);
        panel.add(mapPath);
        panel.add(videoMode);
        panel.add(video);
        add(panel);

        setBounds(500, 300, 700, 700);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}
